//
// Created by sunha on 18-2-20.
//

#ifndef LIST_LINKED_LIST_H
#define LIST_LINKED_LIST_H

#include <memory>
#include <stdexcept>
#include <cstddef>

template <typename T>
class list;

/**
 * The base data in list
 * Users should not create instance from this class
 * Usage:
 * 		The default constructor will create an node with data constructed by default and null-ptr
 * 		Use data to create instance is more reasonable
 * */
template<typename T>
struct list_node{
public:
	typedef T value_type;
	typedef T* pointer;
	typedef T& reference;
	typedef const T& const_reference;
	typedef const T* const_pointer;

	list_node* pre;
	list_node* next;
	T data;

	list_node():pre(nullptr),next(nullptr){}

	explicit list_node(const T& e):pre(nullptr),next(nullptr),data(e){}

	list_node(const list_node<T>& e ):pre(e.pre),next(e.next),data(e.data){}

	//current node should not influence other nodes or something unexpected will happen
	~list_node(){
//		if(this->pre)this->pre->next= nullptr;
//		if(this->next)this->next->pre= nullptr;
	}
};

//point of designing :
// making iterator type using as easily as pointer
template <typename T>
class list_iterator{
public :

	friend class list<T>;

	typedef list_node<T>* node_pointer;
	typedef typename list_node<T>::value_type value_type;
	typedef typename list_node<T>::reference reference;
	typedef typename list_node<T>::pointer pointer;
	typedef typename list_node<T>::const_reference const_reference;
	typedef typename list_node<T>::const_pointer  const_pointer;

	list_iterator():iter(nullptr){}


	explicit list_iterator(const node_pointer e):iter(e){};
	list_iterator(const list_iterator& e ):iter(e.iter){}

	operator node_pointer (){
		return iter;
	}
	operator node_pointer ()const {
		return const_cast<const node_pointer>(iter);
	}
	explicit operator bool()const{
		return iter!= nullptr;
	}

	list_iterator& operator= (const list_iterator& e ){
		iter=e.iter;
	}
	list_iterator& operator= (const node_pointer e ){
		iter=e;
	}

	reference operator* (){
		if(!iter)throw std::out_of_range("Try to * a null-pointer");
		return (*iter).data;
	}
	const_reference operator*()const{
		if(!iter)throw std::out_of_range("Try to * a null-pointer");
		return (*iter).data;
	}

	const_pointer operator->()const {
		return &(operator*());
	}
	pointer operator->(){
		return &(operator*());
	}

	list_iterator & operator++()  {
		if(iter)
		this->iter=(*iter).next;
		else throw std::out_of_range("Try to operator on a nullptr");

		return *this;
	}

	const list_iterator & operator++() const {
		if(iter)
			this->iter=(*iter).next;
		else throw std::out_of_range("Try to operator on a nullptr");

		return *this;
	}
	list_iterator& operator--() {
		if (iter)
			this->iter = (*iter).pre;
		else throw std::out_of_range("Try to operator on a nullptr");

		return *this;
	}
	const list_iterator& operator--() const {
		if (iter)
			this->iter = (*iter).pre;
		else throw std::out_of_range("Try to operator on a nullptr");

		return *this;
	}
	list_iterator operator++(int){
		list_iterator temp=*this;
		++*this;
		return temp;
	}

	const list_iterator operator++(int) const {
		const list_iterator temp=*this;
		++*this;
		return temp;
	}
	list_iterator operator-- (int){
		list_iterator temp=*this;
		--*this;
		return temp;
	}

	const list_iterator operator-- (int)const {
		const list_iterator temp=*this;
		--*this;
		return temp;
	}

	bool operator== (const list_iterator& e)const {
		return this->iter==e.iter;
	}
	bool operator!= (const list_iterator& e )const {
		return !(*this==e);
	}

private :

	mutable node_pointer iter;
};




template <typename T>
class list {
public:
	typedef list_iterator<T> iterator;
	typedef const list_iterator<T> const_iterator;
	typedef typename list_iterator<T>::reference reference;
	typedef typename list_iterator<T>::value_type value_type;
	typedef typename list_iterator<T>::pointer pointer;
	typedef typename list_iterator<T>::const_pointer const_pointer;
	typedef typename list_iterator<T>::const_reference const_reference;
	//construct a head node
	list():data(new list_node<T>),count(0){
		data->next=data;
		data->pre=data;
		first=data;
		last=data;
	}

	explicit list(std::size_t  n,const T& e=T() ):data(new list_node<T>),count(n){
		auto temp=data;
		while(n--){
				temp->next=new list_node<T>(e);
				temp->next->pre=temp;
				temp=temp->next;
			}

		temp->next=data;
		data->pre=temp;

		first=data->next;
		last=temp->next;
	}
	~list(){
		if(first!=data) {
			while (last != first) {
				delete (last--);
			}
			delete (first);
		}
		delete(data);
	}

	explicit list(const std::initializer_list<T>& e ):data(new list_node<T>),count(e.size()){
		list_node<T>* temp(data);
		for(const auto &i: e) {
			temp->next = new list_node<T>(i);
			temp->next->pre=temp;
			temp=temp->next;
		}
		temp->next=data;
		data->pre=temp;
		first=data->next;
		last=temp->next;
	}

	list(const list<T>& e):data(new list_node<T>()),count(e.count){

		list_node<T>* temp_data=data;
		for(auto i=e.first;i!=e.last;i++){
			temp_data->next=new list_node<T>(*i);
			temp_data->next->pre=temp_data;
			temp_data=temp_data->next;
		}
		temp_data->next=data;
		data->pre=temp_data;
		first=data->next;
		last=temp_data->next;
	}

	list(list<T>&& e)noexcept :data(e.data),count(e.count),first(e.first),last(e.last){
		e.data=e.first=e.last=new list_node<T>();
		e.count=0;
	}

	list<T>& operator=(const list<T>& e )&{
		if(this!=&e){
			free();
			auto temp=data;
			for(auto i=e.first;i!=e.last;i++){
				temp->next=new list_node<T>(*i);
				temp->next->pre=temp;
				temp=temp->next;
			}
			temp->next=data;
			data->pre=temp;
			first=data->next;
			last=temp->next;
			count=e.count;
		}
		return *this;

	}
	list<T>& operator= (list<T>&& e )&{
		free();
		data->next=e.first;
		first=e.first;
		last=e.last;
		count=e.count;

		e.first=e.last=e.data=new list_node<T>();
		e.count=0;
		return *this;
	}

	iterator begin()  {
		//if(first==data)return iterator(nullptr);
		return first;
	}
	iterator end() {
		//if(first==last)iterator(nullptr);
		return last;
	}
	const_iterator& begin()const {
		if(first==data)return iterator(nullptr);
		return first;
	}

	const iterator last_()const {
		if(first==last)return iterator();
		return last;}
	iterator pre(const iterator e )const {
		if(!e||(first==data))iterator(nullptr);
		else if(e.iter->pre==data)iterator(nullptr);
		else
		return (iterator) e.iter->pre;
	}
	list<T>& push_back(const T& e ){
		auto temp=new list_node<T>(e);
		temp->next=last;
		temp->pre=last.iter->pre;
		last.iter->pre->next=temp;
		last.iter->pre=temp;
		first=data->next;
		count++;
		return *this;
	}
	list<T>& pop_back(){

		list_node<T>* temp=(last.iter)->pre;
		if(temp!=data){
			temp->pre->next=temp->next;
			temp->next->pre=temp->pre;
			delete(temp);
			count--;
		}
		else throw std::out_of_range("Try to pop element on an empty list");
		return *this;
	}
	std::size_t size(){
		return count;
	}
	reference get_top(){
		if(last==first)throw std::out_of_range("Empty list");
		return *(last.iter->pre);
	}
	bool empty(){
		return first==data;
	}
//	bool inserLast(list<T> &e){
//		e.first.iter->pre=last.iter->pre;
//		last.iter->pre->next=e.first.iter;
//		e.last.iter->pre->next=e.last.iter;
//	}
private :

	void free(){
		if(first!=data) {
			while (last != first) {
				delete (last--);
			}
			delete (first);
		}
	}
	list_node<T>* data;

	iterator first;
	iterator last;

	std::size_t count;
};
template<typename T>
std::ostream& operator<< (std::ostream& out,const list<T>& e ){
	for(auto i=e.begin();i!=e.last_();i++)
		out<<*i;
	return out;
}
#endif //LIST_LINKED_LIST_H
