//
// Created by sunha on 18-3-4.
//

#ifndef REGEX_NFA_H
#define REGEX_NFA_H
#include <string>
#include <vector>
using std::string;

class Gragh;
class netNode;

struct ArcNode{
public:
	//use list to manage VexNode
	typedef netNode* iterator;
	iterator index;
	char info;
	ArcNode* next;

	ArcNode():index(nullptr),info('`'),next(nullptr){}
	ArcNode(iterator iterator1,char inf,ArcNode* po= nullptr):index(iterator1),info(inf),next(po){}
	ArcNode(const ArcNode& e)=default;
	~ArcNode()= default;
};


struct VexNode{
public:
	typedef netNode* iterator;
	typedef ArcNode* ArcList;

	VexNode(){
		Arc=new ArcNode();
	}
	~VexNode(){
		if(Arc) {
			auto temp=Arc->next;
			while (temp) {
				auto _temp = temp;
				temp= temp->next;
				delete (_temp);
			}
			delete(Arc);
		}
	}

	VexNode(const VexNode& e)= default;

	void insert_begin(iterator index,char info){
		auto temp=new ArcNode(index,info);
		auto temp_head=Arc->next;
		Arc->next=temp;
		temp->next=temp_head;
	}
	ArcList begin(){
		return Arc->next;
	}
	ArcList end(){
		return nullptr;
	}
	ArcList & get_data(){
		return Arc;
	}
private :
	ArcList Arc;
};
class netNode{
public:
	netNode*next;
	VexNode data;
	netNode():next(nullptr),data(VexNode()){}

};
class Gragh{
public:
	typedef netNode* iterator;
	Gragh(char info);
	~Gragh(){
		if(data){
			while(data){
				auto temp=data;
				data=data->next;
				delete(temp);
			}
		}
	}
	void mergeNet(Gragh& e);
	void linkNet(Gragh& e);
	void mergeAndLinkNet();
	iterator begin(){return _first;}
	iterator end(){return _last;}
	void giveUp(){data=_first=_last= nullptr;}
private :
	netNode* data;
	iterator _first;
	iterator _last;
};

class NFA {
public:
	typedef Gragh::iterator iterator;
	explicit NFA(char e):
			structure(e){
		beginState=structure.begin();
		endState=structure.end();
	}
	~NFA()= default;

	NFA& operator+=(NFA&);
	NFA& operator|(NFA&);
	NFA&operator*();

	bool pattern(const string& e);
	void giveUp(){
		structure.giveUp();
		beginState=endState= nullptr;
	}
private:
	void emptyMove(std::vector<iterator> &);
	void spMove(std::vector<iterator> & ,char  );

	Gragh structure;
	iterator beginState;
	iterator endState;
};
#endif //REGEX_NFA_H
