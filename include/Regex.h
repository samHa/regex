//
// Created by sunha on 18-3-4.
//
#ifndef REGEX_REGEX_H
#define REGEX_REGEX_H

#include <memory>
#include "NFA.h"
#include <string>
using std::string;

class Regex {
public:
	Regex(const string& );
	bool pattern(const string * );
private:
	bool find(const string&e ,char target);
	NFA* nfa;
};


#endif //REGEX_REGEX_H
