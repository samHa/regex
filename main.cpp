#include <iostream>
#include "NFA.h"
#include <queue>
using std::queue;
int main(int argc,char** argv) {
	NFA a('1');
	NFA b('0');
	NFA c('@');
	NFA e('.');
	NFA f('c');
	NFA g('o');
	NFA h('m');
	NFA last('q');
	NFA last1('q');
	a+=b;
	*a;
	a|c;
	a|last;
	a|last1;
	a|e;
	a|f;
	a|g;
	a|h;
	if(argc<2)return 0;

	for(int i=1;i<argc;i++)
	if(a.pattern(argv[i]))std::cout<<"Correct for :"<<argv[i]<<std::endl;
	else std::cerr<<"Wrong for "<<argv[i]<<std::endl;
	return 0;
}