# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/sunha/CLionProjects/Regex/main.cpp" "/home/sunha/CLionProjects/Regex/cmake-build-debug/CMakeFiles/Regex.dir/main.cpp.o"
  "/home/sunha/CLionProjects/Regex/src/NFA.cpp" "/home/sunha/CLionProjects/Regex/cmake-build-debug/CMakeFiles/Regex.dir/src/NFA.cpp.o"
  "/home/sunha/CLionProjects/Regex/src/Regex.cpp" "/home/sunha/CLionProjects/Regex/cmake-build-debug/CMakeFiles/Regex.dir/src/Regex.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "OPENCV_TRAITS_ENABLE_DEPRECATED"
  "USE_CUDNN"
  "USE_LEVELDB"
  "USE_LMDB"
  "USE_OPENCV"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../include"
  "/usr/local/include"
  "/usr/include/hdf5/serial"
  "/usr/local/cuda-8.0/include"
  "/usr/local/include/opencv"
  "/usr/include/atlas"
  "/home/sunha/Project/caffe-master/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
