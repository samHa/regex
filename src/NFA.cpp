//
// Created by sunha on 18-3-4.
//
#include "NFA.h"
#include <vector>
#include <list>
using std::vector;
using std::list;

Gragh::Gragh(char info):data(new netNode()) {
	_first=data;
	data->next=new netNode();
	data->next->next=new netNode();
	_last=data->next->next;
	_first->data.insert_begin(data->next,info);
	_first->next->data.insert_begin(_last,' ');
}
void Gragh::mergeNet(Gragh &e) {
	auto temp=new netNode();
	temp->next=_first;
	temp->data.insert_begin(_first,' ');
	temp->data.insert_begin(e._first,' ');
	_last->next=e._first;
	auto temp2=new netNode();
	e._last->next=temp2;
	_last->data.insert_begin(temp2,' ');
	e._last->data.insert_begin(temp2,' ');
	_first=temp;
	_last=temp2;
	e.data= e._first=e._last=nullptr;
}
void Gragh::linkNet(Gragh &e) {
	_last->next=e._first;
	_last->data.insert_begin(e._first,' ');
	_last=e._last;
	e.data=e._first=e._last= nullptr;
}
void Gragh::mergeAndLinkNet() {
	auto temp=new netNode();

	temp->next=_first;
	temp->data.insert_begin(_first,' ');

	auto temp_end=new netNode();
	_last->next=temp_end;
	_last->data.insert_begin(temp_end,' ');
	temp_end->data.insert_begin(temp,' ');

	auto temp_end2=new netNode();
	temp_end->next=temp_end2;
	temp->data.insert_begin(temp_end2,' ');
	temp_end->data.insert_begin(temp_end2,' ');

	_first=temp;
	_last=temp_end2;
}
NFA& NFA::operator+=(NFA & e) {
		this->structure.mergeNet(e.structure);
		beginState=structure.begin();
		endState=structure.end();
}
NFA& NFA::operator|(NFA &e) {
	this->structure.linkNet(e.structure);
	this->endState=structure.end();
}
NFA& NFA::operator*() {
	this->structure.mergeAndLinkNet();
	beginState=structure.begin();
	endState=structure.end();
}
void NFA::emptyMove(vector<iterator> &currentState) {
	list<iterator > temp_list(currentState.begin(),currentState.end());
	for(auto j=temp_list.begin();j!=temp_list.end();j++){
		auto temp=(*j)->data.get_data()->next;
		while(temp){
			if(temp->info==' ')
			{
				temp_list.push_back(temp->index);
			}
			temp=temp->next;
		}
	}
	currentState.clear();
	currentState.insert(currentState.begin(),temp_list.begin(),temp_list.end());
}
void NFA::spMove(std::vector<iterator> &currentState, char element) {
	vector<iterator> temp_v;
	for(auto j=currentState.begin();j!=currentState.end();j++){
		auto temp=(*j)->data.get_data()->next;
		while(temp){
			if(temp->info==element)
			{
				temp_v.push_back(temp->index);
			}
			temp=temp->next;
		}
	}
	currentState=std::move(temp_v);
}
bool NFA::pattern(const string &e) {
	vector<iterator> currentState;
	currentState.push_back(this->beginState);
	for(auto i=e.begin();i!=e.end();i++){
		emptyMove(currentState);
		spMove(currentState,*i);
	}
	emptyMove(currentState);
	for(auto j :currentState){
		if(j==endState)
			return true;
	}
	return false;
}